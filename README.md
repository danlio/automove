# AutoMove
將資料夾內的檔案自動根據日期分配,

資料夾格式:

年/年-月/年-月-日/檔案

## 初始設定

# clone 專案後
```
cd automove
cp .env.example .env
vi .env

```
# Vi .env 將以下資料輸入
```
#偵測的資料夾
AUTOPATH=
#移動到的資料夾
MOVEPATH=
#Telegram
TELEGRAM_TOKEN=
TELEGRAM_CHAIT_ID=
CURL_TIMEOUT=6000
```
# 執行
```
node main.js
```
