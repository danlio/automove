const { count } = require('console');

require('dotenv').config();

let Telegram = new function () {
  const axios = require("axios");
  const moment = require('moment');
  const $this = this;
  const Token = process.env["TELEGRAM_TOKEN"];
  const chat_id = process.env["TELEGRAM_CHAIT_ID"];
  let instance = axios.create();

  $this.SetMessage = function (msg) {
    let TitleMsg = '[AutoMove] ' + msg;
    instance(
      {
        url: 'https://api.telegram.org/bot' + Token + '/sendMessage?chat_id=' + chat_id + '&text=' + TitleMsg,
        method: 'get',
      },
      {
        timeout: process.env['CURL_TIMEOUT']
      }
    ).then(function (response) {
      //console.log(response)
    })
    .catch(function (error) {
      console.log(error)
    });
  }

  $this.init = function () {
    let nowTime = '[' + moment(moment().valueOf()).format('YYYY-MM-DD HH:mm:ss') + ']';
    console.log(nowTime, 'Telegram Runing.');
    //$this.SetMessage(nowTime + ' Telegram Runing.')
  }
  $this.init();
}

let main = new function () {
  const $this = this;
  const fs = require('fs');
  const fs_e = require('fs-extra');
  const moment = require('moment');
  const AutoPath = process.env["AUTOPATH"];
  const MovePath = process.env["MOVEPATH"];

  $this.GetDirFile = function () {
    fs.readdir(AutoPath, (err, files) => {
      console.log(files.length, files);
      let nowTime = '[' + moment(moment().valueOf()).format('YYYY-MM-DD HH:mm:ss') + ']';
      Telegram.SetMessage(nowTime + ' Files : ' + (files.length - 1));
      if (files.length > 1 ) {
        files.forEach(filename => {
          if (filename != '@eaDir') {
            console.log(AutoPath + filename)
            fs.stat(AutoPath + filename,
              async function (err, stat) {
                if (!stat.isDirectory()) {
                  if (err) {
                    return console.log("err",err, stat);
                  }
                  /** 檔案時間
                   * atime "访问时间": 上次访问文件数据的时间。 由 mknod(2)、 utimes(2) 和 read(2) 系统调用更改。
									 * mtime "修改时间": 上次修改文件数据的时间。 由 mknod(2)、 utimes(2) 和 write(2) 系统调用更改。
									 * ctime "更改时间": 上次更改文件状态（修改索引节点数据）的时间。
									 * 				由 chmod(2)、 chown(2)、 link(2)、 mknod(2)、 rename(2)、 unlink(2)、 utimes(2)、 read(2) 和 write(2)
									 * 				系统调用更改。
									 * birthtime "创建时间": 文件创建时间。
									 * 				创建文件时设置一次。
									 * 				在创建时间不可用的文件系统上，该字段可能改为保存 ctime 或 1970-01-01T00:00Z（即 Unix 纪元时间戳 0）。
									 * 				在这种情况下，该值可能大于 atime 或 mtime。
									 * 				在 Darwin 和其他 FreeBSD 变体上，如果使用 utimes(2) 系统调用将 atime 显式设置为比当前 birthtime 更早的值，
									 * 				也会被设置。
                   */
                  //let Tmp = moment(stat.birthtime);
                  let Tmp = moment(stat.mtime);
                  let fileObj = {
                    dataMoment: Tmp,
                    path: Tmp.format('YYYY') + '/' + Tmp.format('YYYY-MM') + '/' + Tmp.format('YYYY-MM-DD') + '/',
                    name: filename
                  }
                  await CheckAndCreateDir(fileObj);
                } else {
                  console.log(filename, stat)
                }
              }
            );
          } else {

          }
        });
      }
    });
  }

  CheckAndCreateDir = async function (fileObj) {
    for (let key = 0; key < 2; key++) {
      if (!fs.existsSync(MovePath + fileObj.dataMoment.format('YYYY'))) {
        fs.mkdirSync(MovePath + fileObj.dataMoment.format('YYYY'));
      }
      if (!fs.existsSync(MovePath + fileObj.dataMoment.format('YYYY') + '/' + fileObj.dataMoment.format('YYYY-MM'))) {
        fs.mkdirSync(MovePath + fileObj.dataMoment.format('YYYY') + '/' + fileObj.dataMoment.format('YYYY-MM'));
      }
      if (!fs.existsSync(MovePath + fileObj.path)) {
        fs.mkdirSync(MovePath + fileObj.path);
      }
    }
    await MoveFile(fileObj);
  }

  Exists = async function (path) {
    try {
      await fs.access(path)
      return true
    } catch {
      return false
    }
  }

  MoveFile = async function (fileObj) {
    let oldPath = AutoPath + fileObj.name;
    let newPath = MovePath + fileObj.path + fileObj.name;
    let nowTime = '[' + moment(moment().valueOf()).format('YYYY-MM-DD HH:mm:ss') + ']';
    let checkType = await Exists(newPath);
    if (!checkType) {
      fs_e.move(oldPath, newPath, (err) => {
        if(err) {
          console.log(err,oldPath);
          fs_e.unlink(oldPath, (err) => {
            Telegram.SetMessage(nowTime + ' successfully deleted ' + oldPath);
          });
        } else {
          console.log(nowTime, fileObj.path + fileObj.name + ' success');
          Telegram.SetMessage(nowTime + ' '+ fileObj.path + fileObj.name + ' Move success.')
        }
      });
    } else {
      fs_e.unlink(oldPath, (err) => {
        Telegram.SetMessage(nowTime + ' successfully deleted ' + oldPath);
      });
    }
  }

  $this.init = function () {
    let nowTime = '[' + moment(moment().valueOf()).format('YYYY-MM-DD HH:mm:ss') + ']';
    console.log(nowTime, 'Main Runing.');
    Telegram.SetMessage(nowTime + ' Main Runing.')
  }
  $this.init();
}